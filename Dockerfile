FROM alpine:3.10
LABEL author="SWFox"
RUN apk --no-cache add curl
RUN apk --no-cache add tar
ENV JAVA_VERSION jdk8u
RUN curl -LfsSo /tmp/openjdk.tar.gz 'https://api.adoptopenjdk.net/v2/binary/nightly/openjdk8?openjdk_impl=hotspot&os=linux&arch=x64&release=latest&type=jre'
WORKDIR /opt/java/openjdk;
RUN tar -xf /tmp/openjdk.tar.gz;
RUN rm -rf /var/cache/apk/*;
RUN rm -rf /tmp/openjdk.tar.gz;

ENV JAVA_HOME=/opt/java/openjdk \
    PATH="/opt/java/openjdk/bin:$PATH"